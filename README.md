## ignite-and-spark-integration
~~~
sudo mkdir /var/lib/zookeeper
sudo chmod 777 /var/lib/zookeeper
ZOOKEEPER_HOME=/home/tyrbovenik/apache-zookeeper-3.5.6-bin
$ZOOKEEPER_HOME/bin/zkServer.sh start
~~~

~~~
KAFKA_HOME=/home/tyrbovenik/kafka_2.12-2.3.0
$KAFKA_HOME/bin/zookeeper-server-start.sh $KAFKA_HOME/config/zookeeper.properties
$KAFKA_HOME/bin/kafka-server-start.sh $KAFKA_HOME/config/server.properties
~~~

~~~
IGNITE_HOME=/home/tyrbovenik/apache-ignite-fabric-2.3.0-bin
$IGNITE_HOME/bin/ignite.sh examples/config/spark/example-shared-rdd.xml
~~~
~~~
SPARK_HOME=/home/tyrbovenik/spark-2.2.0-bin-hadoop2.7
$SPARK_HOME/sbin/start-master.sh 
$SPARK_HOME/bin/spark-class org.apache.spark.deploy.worker.Worker spark://tyrbovenik-Inspiron-7577:7077
~~~
~~~
export SPARK_MASTER_URL=spark://tyrbovenik-Inspiron-7577:7077
export INGITE_CONFIG_PATH=/home/tyrbovenik/apache-ignite-2.7.6-bin/examples/config/spark/example-shared-rdd.xml
export KAFKA_URL=localhost:9092
export DATA_COUNT=140
export KAFKA_TOPIC_NAME=topic

SPARK_HOME=/home/tyrbovenik/spark-2.2.0-bin-hadoop2.7
$SPARK_HOME/bin/spark-submit --class "com.gridgain.RDDWriter" --master spark://tyrbovenik-Inspiron-7577:7077 "/home/tyrbovenik/programs/java/ignite-and-spark-integration/ignite-rdd/ignite-spark-java/target/ignite-spark-java-1.0-SNAPSHOT-jar-with-dependencies.jar"
$SPARK_HOME/bin/spark-submit --class "com.gridgain.RDDReader" --master spark://tyrbovenik-Inspiron-7577:7077 "/home/tyrbovenik/programs/java/ignite-and-spark-integration/ignite-rdd/ignite-spark-java/target/ignite-spark-java-1.0-SNAPSHOT-jar-with-dependencies.jar"
~~~
- [Apache Ignite and Apache Spark Integration using Ignite RDDs](https://www.gridgain.com/resources/blog/apacher-ignitetm-and-apacher-sparktm-integration-using-ignite-rdds)
- [Apache Ignite and Apache Spark Integration using Ignite DataFrames](https://www.gridgain.com/resources/blog/apacher-ignitetm-and-apacher-sparktm-integration-using-ignite-dataframes)
