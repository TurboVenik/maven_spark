import random

from kafka import KafkaProducer

count = 0
producer = KafkaProducer(bootstrap_servers=['localhost:9092'])


def send_data(priority, hour, day):
    str_data = f'{"{"}"logTime":"Dec {day}, 3919 {hour}:01:00 AM","priority":{priority}{"}"}'
    future = producer.send('topic', key=b'lol18', value=str_data.encode())

    future.get(timeout=10)


for j in range(7):
    for i in range((j + 1) * 5):
        rand_int = random.randint(2, 5)
        send_data(1, rand_int, j)
        count += 1


print(count)
