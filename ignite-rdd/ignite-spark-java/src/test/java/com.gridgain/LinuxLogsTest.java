package com.gridgain;

import com.gridgain.model.LogCounts;
import com.gridgain.model.LogInfo;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.*;

import static org.junit.Assert.*;

public class LinuxLogsTest
{
    private JavaSparkContext sparkCtx;
    private List<LogInfo> input;
    private List<LogCounts> expectedOutput;

    @Before
    public void init() throws IllegalArgumentException, IOException
    {
        // Контекст Spark
        SparkConf conf = new SparkConf();
        conf.setMaster("local[2]");
        conf.setAppName("junit");
        sparkCtx = new JavaSparkContext(conf);

        // Входные данные
        input = new ArrayList<>();

        Date date1 = new Date(1);
        Date date2 = new Date(1000000000);

        for (int i = 0; i < 5; i++) {
            input.add(new LogInfo(date1, 0));
        }
        for (int i = 0; i < 7; i++) {
            input.add(new LogInfo(date2, 0));
        }
        for (int i = 0; i < 10; i++) {
            input.add(new LogInfo(date2, 1));
        }
        // Ожидаемый результат
        expectedOutput = new ArrayList<>();
        expectedOutput.add(new LogCounts(date1, 0, 5));
        expectedOutput.add(new LogCounts(date2, 0, 7));
        expectedOutput.add(new LogCounts(date2, 1, 10));
    }

    @Test
    public void test() {
        // Рассчет
        JavaRDD<LogInfo> logInfoRDD = sparkCtx.parallelize(input, 1);
        JavaRDD<LogCounts> res = RDDReader.calculateCount(logInfoRDD);
        List<LogCounts> reslist = res.collect();
        // Проверка длины ответа и длины ожилаемого ответа
        assertEquals(expectedOutput.size(), reslist.size());
        // Проверка правильности ответа
        assertTrue(reslist.containsAll(expectedOutput));
    }
}