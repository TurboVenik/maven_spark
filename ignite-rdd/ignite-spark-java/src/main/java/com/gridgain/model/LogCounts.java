package com.gridgain.model;

import scala.Serializable;

import java.util.Date;
import java.util.Objects;

// DTO с результатами расчетов
public class LogCounts implements Serializable {

    // Время лога
    private Date logTime;
    // Приоритет лога
    private int priority;
    // Количество логов для часа и приоритета
    private int count;

    public LogCounts(Date logtime, int priority, int count) {
        this.logTime = logtime;
        this.priority = priority;
        this.count = count;
    }

    public Date getLogTime() {
        return logTime;
    }

    public int getPriority() {
        return priority;
    }

    public int getCount() {
        return count;
    }

    @Override
    public boolean equals(Object o) {
        //If objects are located at the same memory location, return true straight away
        if (this == o) return true;
        //If other object is null or represents a different class, return false straight away
        if (o == null || getClass() != o.getClass()) return false;
        //Cast other object to this class
        LogCounts logCounts = (LogCounts) o;
        //Compare objects' member variables
        return priority == logCounts.getPriority() &&
                count == logCounts.getCount() &&
                logTime.equals(logCounts.getLogTime());
    }

    @Override
    public int hashCode() {
        return Objects.hash(logTime, priority, count);
    }

    @Override
    public String toString() {
        return "<" + logTime + "," + priority + "," + count + ">";
    }
}
