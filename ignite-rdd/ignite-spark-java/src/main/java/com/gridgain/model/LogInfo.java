package com.gridgain.model;

import scala.Serializable;

import java.util.Date;
import java.util.Objects;

// DTO с данными о логе
public class LogInfo implements Serializable {

    // Время лога
    private Date logTime;
    // Приоритет лога
    private int priority;

    public void setLogTime(Date logTime) {
        this.logTime = logTime;
    }


    public void setPriority(int priority) {
        this.priority = priority;
    }

    public LogInfo(Date logtime, int priority) {
        this.logTime = logtime;
        this.priority = priority;
    }


    public Date getLogTime() {
        return logTime;
    }


    public int getPriority() {
        return priority;
    }


    @Override
    public boolean equals(Object o) {
        //If objects are located at the same memory location, return true straight away
        if (this == o) return true;
        //If other object is null or represents a different class, return false straight away
        if (o == null || getClass() != o.getClass()) return false;
        //Cast other object to this class
        LogInfo logInfo = (LogInfo) o;
        //Compare objects' member variables
        return logTime.equals(logInfo.getLogTime()) &&
                priority == logInfo.getPriority();
    }


    @Override
    public int hashCode() {
        return Objects.hash(logTime, priority);
    }

    @Override
    public String toString() {
        return "<" + logTime + "," + priority + ">";
    }
}
