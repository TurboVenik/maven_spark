package com.gridgain;

import com.gridgain.model.LogCounts;
import com.gridgain.model.LogInfo;
import org.apache.ignite.spark.JavaIgniteContext;
import org.apache.ignite.spark.JavaIgniteRDD;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import scala.Tuple2;

import java.util.UUID;

public class RDDReader {

    // Получение RDD из Ignite
    private static JavaIgniteRDD<String, LogInfo> getRdd(JavaIgniteContext<String, LogInfo> igniteContext) {
        System.out.println("...:::getRdd:::...");
        return igniteContext.<String, LogInfo>fromCache("sharedRDD");
    }

    public static JavaRDD<LogInfo> mapToLogInfo(JavaIgniteRDD<String, LogInfo> sharedRDD) {
        System.out.println("...:::mapToLogInfo:::...");
        return sharedRDD.map(Tuple2::_2);
    }

    // Расчет количество записей для каждого часа и каждого уровня
    public static JavaRDD<LogCounts> calculateCount(JavaRDD<LogInfo> rdd) {
        System.out.println("...:::calculateCount:::...");
        return rdd
                .mapToPair((info) -> new Tuple2<>(info, 1))
                .reduceByKey(Integer::sum)
                .map((tuple) -> new LogCounts(tuple._1.getLogTime(), tuple._1().getPriority(), tuple._2()));
    }

    // Вывод результата
    private static void printRdd(JavaRDD<LogCounts> map) {
        System.out.println("...:::printRdd:::...");
        System.out.println("The count is " + map.count());
        map.collect().forEach(System.out::println);
    }

    // Сохранение результата в Ignite
    private static void saveToDatabase(JavaIgniteRDD<String, LogInfo> sharedRDD, JavaRDD<LogCounts> map) {
        System.out.println("...:::saveToDatabase:::...");

        // Очистка и создание таблицы с результатом

        sharedRDD.sql("DROP TABLE IF EXISTS PUBLIC.LogCounts");
        sharedRDD.sql("CREATE TABLE IF NOT EXISTS PUBLIC.LogCounts (\n" +
                "  id varchar,\n" +
                "  date varchar,\n" +
                "  priority int,\n" +
                "  count int,\n" +
                "PRIMARY KEY (id)\n" +
                ");");


        System.out.println("CREATED");
        // Вывод таблицы до вставки данных
        Dataset df = sharedRDD.sql("select * from PUBLIC.LogCounts");
        df.show();

        // Создание запроса на вставку
        StringBuilder sb = new StringBuilder();
        sb.append("INSERT INTO PUBLIC.LogCounts (id, date, priority, count) VALUES");

        map.collect().forEach((value) -> sb.append(String.format(
                "('%s', '%s', %s, %s),",
                UUID.randomUUID(),
                value.getLogTime(),
                value.getPriority(),
                value.getCount())));
        String insert_stmt = sb.toString();
        // Вставка данных в Ignite
        sharedRDD.sql(insert_stmt);
        // Запрос и вывод данных из талицы, созданной ранее
        df = sharedRDD.sql("select DATE, PRIORITY, COUNT  from PUBLIC.LogCounts");
        df.show();
    }

    public static void main(String[] args) {
        // Адрес мастера Spark из переменной окружения
        String sparkMasterUrl = System.getenv("SPARK_MASTER_URL");
        // Адреса пути до конфигурации Ignite из переменной окружения
        String igniteConfig = System.getenv("INGITE_CONFIG_PATH");

        System.out.println("SPARK_MASTER_URL: " + sparkMasterUrl);
        System.out.println("INGITE_CONFIG_PATH: " + igniteConfig);

        // Конфигурация spark
        SparkConf sparkConf = new SparkConf()
                .setAppName("RDDReader")
                .setMaster(sparkMasterUrl)
                .set("spark.executor.instances", "2");
        // Создание контекста spark
        JavaSparkContext sparkContext = new JavaSparkContext(sparkConf);
        // Настройки логов
        Logger.getRootLogger().setLevel(Level.OFF);
        Logger.getLogger("org.apache.ignite").setLevel(Level.OFF);
        // Создание контекста ignite
        JavaIgniteContext<String, LogInfo> igniteContext = new JavaIgniteContext<String, LogInfo>(
                sparkContext, igniteConfig, true);
        // Получение IgniteRdd
        JavaIgniteRDD<String, LogInfo> sharedRDD = getRdd(igniteContext);

        // Расчет количество записей для каждого часа и каждого уровня
        JavaRDD<LogCounts> map = calculateCount(mapToLogInfo(sharedRDD));
        // Вывод результата
        printRdd(map);
        // Сохранение результата в Ignite
        saveToDatabase(sharedRDD, map);
        // Закрытие контекста
        igniteContext.close(true);
        sparkContext.close();
    }
}