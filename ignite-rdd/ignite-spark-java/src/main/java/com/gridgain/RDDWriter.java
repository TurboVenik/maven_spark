package com.gridgain;

import com.google.gson.Gson;
import com.gridgain.model.LogInfo;
import org.apache.ignite.spark.JavaIgniteContext;
import org.apache.ignite.spark.JavaIgniteRDD;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import java.util.*;
import org.apache.spark.streaming.kafka010.*;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.serialization.StringDeserializer;

// Код, создающий ignite RDD
public class RDDWriter {


    public static void main(String[] args) {
        Gson gson = new Gson();

        // Адрес мастера Spark из переменной окружения
        String sparkMasterUrl = System.getenv("SPARK_MASTER_URL");
        // Адреса пути до конфигурации Ignite из переменной окружения
        String igniteConfig = System.getenv("INGITE_CONFIG_PATH");
        // Количество данных из переменной окружения
        System.out.println(System.getenv("DATA_COUNT"));
        int dataCount = Integer.parseInt(System.getenv("DATA_COUNT"));
        // Количество данных из переменной окружения
        String kafkaURL = System.getenv("KAFKA_URL");
        // Количество данных из переменной окружения
        String kafkaTopicName = System.getenv("KAFKA_TOPIC_NAME");

        System.out.println("SPARK_MASTER_URL: " + sparkMasterUrl);
        System.out.println("INGITE_CONFIG_PATH: " + igniteConfig);
        System.out.println("DATA_COUNT: " + dataCount);
        System.out.println("KAFKA_URL: " + kafkaURL);
        System.out.println("KAFKA_TOPIC_NAME: " + kafkaTopicName);

        // Конфигурация spark
        SparkConf sparkConf = new SparkConf()
                .setAppName("RDDWriter")
                .setMaster(sparkMasterUrl)
                .set("spark.executor.instances", "2");
        // Создание контекста spark
        JavaSparkContext sparkContext = new JavaSparkContext(sparkConf);
        // Настройки логов
        Logger.getRootLogger().setLevel(Level.OFF);
        Logger.getLogger("org.apache.ignite").setLevel(Level.OFF);
        // Создание контекста ignite
        JavaIgniteContext<String, LogInfo> igniteContext = new JavaIgniteContext<String, LogInfo>(
                sparkContext, igniteConfig, true);

        // Параметры Kafka
        Map<String, Object> kafkaParams = new HashMap<>();
        kafkaParams.put("bootstrap.servers", kafkaURL);
        kafkaParams.put("key.deserializer", StringDeserializer.class);
        kafkaParams.put("value.deserializer", StringDeserializer.class);
        kafkaParams.put("group.id", "use_a_separate_group_id_for_each_stream");
        kafkaParams.put("auto.offset.reset", "latest");
        kafkaParams.put("enable.auto.commit", false);
        OffsetRange[] offsetRanges = {OffsetRange.create(kafkaTopicName, 0, 0, dataCount)};

        // Создание RDD из данных kafka
        JavaRDD<ConsumerRecord<String, String>> rdd = KafkaUtils.createRDD(
                sparkContext,
                kafkaParams,
                offsetRanges,
                LocationStrategies.PreferConsistent()
        );

        // Проверка количества данных
        System.out.println(rdd.count());

        // Создание ignite RDD
        List<String> collect = rdd.map(ConsumerRecord::value).collect();
        List<LogInfo> data = new ArrayList<>();

        for (String s : collect) {
            LogInfo logInfo = gson.fromJson(s, LogInfo.class);
            data.add(logInfo);
        }

        JavaIgniteRDD<String, LogInfo> sharedRDD = igniteContext.<String, LogInfo>fromCache("sharedRDD");
        // Проверка количества данных
        System.out.println("LENGTH: " + data.size());
        JavaRDD<LogInfo> javaRDD = sparkContext.<LogInfo>parallelize(data);
        // Запись данных в Ignite RDD
        sharedRDD.saveValues(javaRDD);
        // Закрытие контекста
        igniteContext.close(true);
        sparkContext.close();
    }
}